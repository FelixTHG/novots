import React, {Fragment, useState} from 'react';
import './App.css';
import {Login} from './modals/Login';
import {UploadPreview} from './modals/UploadPreview';
import {Focus} from './modals/Focus';
// import * as NovoRender from "https://api.novorender.com/scripts/v0.0.1/index.js";


function App() {
  // const { isShown, toggle } = useModal();
  const [loginIsShown, setLoginIsShown] = useState<boolean>(false);
  const [previewUploadIsShown, setPreviewUploadIsShown] = useState<boolean>(false);
  const [focusIsShown, setFocusIsShown] = useState<boolean>(false);
    
  const outputCanvas : JSX.Element = <canvas id="outputCanvas"></canvas>;

  // main(outputCanvas);
  // async function main(outputCanvas : JSX.Element) {
  //     const api = NovoRender.createAPI();
  //     const view = api.createView(outputCanvas, { background: { color: [0, 0, 0.25, 1] } });
  //     view.scene = await api.loadScene(NovoRender.WellKnownSceneIds.cube);
  //     view.camera.controller = api.createCameraController({ kind: "turntable" });
  // }



  return (
    <div className="App">
      {/* I should have created a modal-component that would show the correct
      form based on what button is clicked, in stead of creating multiple separate ones.
      that way I would have less redundant code */}
      <button onClick={() => setLoginIsShown(!loginIsShown)}>Open login</button>
      <button onClick={() => setPreviewUploadIsShown(!previewUploadIsShown)}>Upload for preview</button>
      <button onClick={() => setFocusIsShown(!focusIsShown)}>Add To Focus</button>
      <Login isShown={loginIsShown} hide={() => setLoginIsShown(!loginIsShown)} headerText="Login Register"/> 
      <UploadPreview isShown={previewUploadIsShown} 
      hide={()=> setPreviewUploadIsShown(!previewUploadIsShown)}
      headerText="Preview"/>
      <Focus isShown={focusIsShown} 
      hide={() => setFocusIsShown(!focusIsShown)} 
      headerText="Add To Focus" />
      
      <Fragment>
        {outputCanvas}
      </Fragment>
    </div>
  );
}

export default App;
