//hardcode object that is in localstorage that allows user to login
import React, { FunctionComponent } from 'react';
import ReactDOM from 'react-dom';
import {ModalProps} from './IModal';

export const UploadPreview: FunctionComponent<ModalProps> = ({
  isShown,
  hide,
  // modalContent,
  headerText,
}) => {
  const modal = (
    <React.Fragment>
      <div className="backdrop"></div>
      <div className="cssModal">
        <header className="modalHeader">
          <p>{headerText}</p>
          <button className="btnCloseModal" onClick={hide}>X</button>
        </header>
        <section className="modalContent">
          {/* this div should've been a component :( */}
          <div>
            <form onSubmit={(e) => e.preventDefault()}>
              <label>Pick a file to upload</label> <br/>
              <input type="file"/><br/>
              <input type="submit" value="send"/>
            </form>
          </div>
        </section>
        <footer className="modalFooter">
          <p>footer</p>
        </footer>
      </div>
    </React.Fragment>
  );
  return isShown ? ReactDOM.createPortal(modal, document.body) : null;
};
