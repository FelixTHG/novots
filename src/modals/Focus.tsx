//hardcode object that is in localstorage that allows user to login
import React, { FunctionComponent } from 'react';
import ReactDOM from 'react-dom';
import {ModalProps} from './IModal';

export const Focus: FunctionComponent<ModalProps> = ({
  isShown,
  hide,
  // modalContent,
  headerText,
}) => {
  // let shownTab : string = tabsArray[0];
  const modal = (
    <React.Fragment>
      <div className="backdrop"></div>
      <div className="cssModal">
        <header className="modalHeader">
          <p>{headerText}</p>
          <button className="btnCloseModal" onClick={hide}>X</button>
        </header>
        <section className="modalContent">
          {/* this div should've been a component :( */}
          <div>
            <form onSubmit={(e) => e.preventDefault()}>
              <p>adding your currently selected element to the list of focused elements</p>
              <label>Reason for focus:</label> <br/>
              {/* would be nice if this text-area's size changed dynamically */}
              <textarea cols={30} rows={15} placeholder={descriptionExample}></textarea>
              <p>max 450 characters</p>
              <label>Who should look at this?</label><br/>
              <select defaultValue={responsibles[0]}>
                <option value={responsibles[0]}>Construction engineer</option>
                <option value={responsibles[1]}>Electrical engineer</option>
                <option value={responsibles[2]}>Well technician</option>
                <option value={responsibles[3]}>HMS advisor</option>
              </select>
              <input type="submit" value="add"/>
            </form>
          </div>
        </section>
        <footer className="modalFooter">
          <p>footer</p>
        </footer>
      </div>
    </React.Fragment>
  );
  return isShown ? ReactDOM.createPortal(modal, document.body) : null;
};


const responsibles : Array<string> = [ "Construction", "Electrical", "Well technician", "HMS advisor"];
const descriptionExample : string = "Describe why this needs to be looked at, for example: \nHere there needs to be a hole in the wall for a ventilation shaft. We need a construction engineer to figure out where it's safest/best to make a hole."