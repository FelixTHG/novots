export interface ModalProps {
  isShown: boolean;
  hide: () => void;
  // modalContent: JSX.Element;
  headerText: string;
  // cssSelector: string;
}