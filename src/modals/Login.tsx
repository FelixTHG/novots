//hardcode object that is in localstorage that allows user to login
import React, { FunctionComponent, useState } from 'react';
import {LoginForm} from "./../components/LoginForm";
import ReactDOM from 'react-dom';
import { RegisterForm } from '../components/RegisterForm';
import {ModalProps} from './IModal';

export const Login: FunctionComponent<ModalProps> = ({
  isShown,
  hide,
  // modalContent,
  headerText,
}) => {
  const tabsArray = headerText.split(' ');
  const [shownTab, setShownTab] = useState<string>(tabsArray[0]);
  // let shownTab : string = tabsArray[0];
  const modal = (
    <React.Fragment>
      <div className="backdrop"></div>
      <div className="cssModal">
        <header className="modalHeader">
          {/* create state that keeps track of shownTab and set it in a function */}
          <button className={ shownTab === tabsArray[0] ? "isShownTab" : "isNotShownTab"} onClick={ () => {setShownTab(tabsArray[0]) }}>{tabsArray[0]}</button>
          <button className={ shownTab === tabsArray[1] ? "isShownTab" : "isNotShownTab"} onClick={ () => {setShownTab(tabsArray[1]) }}>{tabsArray[1]}</button>
          <button className="btnCloseModal" onClick={hide}>X</button>
        </header>
        <section className="modalContent">
          {shownTab === tabsArray[0] ? <LoginForm/> : <RegisterForm/>}
        </section>
        <footer className="modalFooter">
          <p>footer</p>
        </footer>
      </div>
    </React.Fragment>
  );
  return isShown ? ReactDOM.createPortal(modal, document.body) : null;
};
