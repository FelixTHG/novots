import React, {FunctionComponent} from "react";

export const RegisterForm: FunctionComponent = () => {
  return(
    <div className="cssFormWrapper">
      <form onSubmit={(e) => e.preventDefault()}>
        <label>create username: </label> <br/>
        <input type="text" placeholder="username"/> <br/>
        <label>provide an e-mail: </label> <br/>
        <input type="email" placeholder="e-mail"/> <br/>
        <label>create password: </label> <br/>
        <input type="password" placeholder="password"/> <br/>
        <label>repeat password: </label> <br/>
        <input type="password" placeholder=""/> <br/>
        <input className="submitButton" type="submit" value="Register"/>
      </form>

    </div>
  )
}