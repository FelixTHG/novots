import React, {FunctionComponent} from "react";

// export interface LoginFormProps {
//   isShownTab: boolean;
//   // isShown: boolean;
//   // hide: () => void;
//   // modalContent: JSX.Element;
//   // headerText: string;
// }

export const LoginForm: FunctionComponent = () => {
  return(
    <div className="cssFormWrapper">
      <form onSubmit={(e) => e.preventDefault()}>
        <label>your username: </label> <br/>
        <input type="text" placeholder="username"/> <br/>
        <label>your password: </label> <br/>
        <input type="text" placeholder="password"/> <br/>
        <input className="submitButton" type="submit" value="log in"/>
      </form>

    </div>
  )
}